package models

type ApiResponce struct {
	PolicyActions []struct {
		PolicyTypeCode          string      `json:"policy_type_code"`
		PolicyTypeDisplay       string      `json:"policy_type_display"`
		FlagValueDisplayField   string      `json:"flag_value_display_field"`
		PolicyValueDisplayField string      `json:"policy_value_display_field"`
		Policyvalue             interface{} `json:"policyvalue"`
		Flagged                 interface{} `json:"flagged"`
		Notes                   interface{} `json:"notes"`
	} `json:"policyActions"`
	StringencyData struct {
		DateValue        string  `json:"date_value"`
		CountryCode      string  `json:"country_code"`
		Confirmed        int     `json:"confirmed"`
		Deaths           int     `json:"deaths"`
		StringencyActual float64 `json:"stringency_actual"`
		Stringency       float64 `json:"stringency"`
		Msg              string  `json:"msg"`
	} `json:"stringencyData"`
}

type DataInput struct {
	DateValue        string  `json:"date_value" binding:"required"`
	CountryCode      string  `json:"country_code" binding:"required"`
	Confirmed        int     `json:"Confirmed" binding:"required"`
	Deaths           int     `json:"deaths" binding:"required"`
	StringencyActual float64 `json:"stringency_actual" binding:"required"`
	Stringency       float64 `json:"stringency" binding:"required"`
}
