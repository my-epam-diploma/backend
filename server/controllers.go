package server

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/my-epam-diploma/backend/client"
	db "gitlab.com/my-epam-diploma/backend/database"
	"gitlab.com/my-epam-diploma/backend/models"
)

func findAll(c *gin.Context) {
	var records []models.DataInput
	db.DB.Find(&records)

	c.JSON(http.StatusOK, gin.H{"data": records})
}

func findByDate(c *gin.Context) {
	var responce []models.DataInput
	search := db.DB.Where(&models.DataInput{DateValue: c.Param("date")}).Find(&responce)
	if search.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	c.Header("Access-Control-Allow-Origin", "*")
	c.JSON(http.StatusOK, gin.H{"data": responce})
}

func updateRequest(c *gin.Context) {
	noDataCountry := client.RequestData()
	c.Header("Access-Control-Allow-Origin", "*")
	c.JSON(http.StatusOK, gin.H{"no_data": noDataCountry})
}

func healthz(c *gin.Context) {
	c.String(http.StatusOK, "Presentetion!")
}
