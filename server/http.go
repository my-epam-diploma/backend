package server

import (
	"github.com/gin-gonic/gin"
)

var api = "/api/v1/"

func StartServer() {
	//gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	r.GET(api+"/everything", findAll)
	r.GET(api+"/bydate/:date", findByDate)
	r.GET(api+"/update", updateRequest)
	r.GET(api+"/healthz", healthz)

	r.Run(":8080")
}
