FROM golang:1.17 as builder
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o backend .

FROM alpine:latest
RUN apk --no-cache add ca-certificates curl
RUN adduser -D backend
COPY --from=builder --chown=backend /app/backend /
USER backend
EXPOSE 8080
CMD ["/backend"]