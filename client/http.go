package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	db "gitlab.com/my-epam-diploma/backend/database"
	"gitlab.com/my-epam-diploma/backend/models"
)

var startDate = time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
var endDate = time.Now()
var urlApi = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/actions/"
var countries = []string{"POL", "NOR", "BEL", "AUT", "BGR", "CZE", "FIN", "GRC", "ITA", "NLD"}

func RequestData() map[string][]string {
	datesRange := DatesRange(startDate, endDate)
	noDataCountry := map[string][]string{}
	for _, date := range datesRange {
		for _, country := range countries {
			if CheckDBForRecord(country, date) {
				url := urlApi + country + "/" + date
				response := apiGet(url)
				if strings.Contains(response.StringencyData.Msg, "Data unavailable") || response.StringencyData.Confirmed == 0 {
					fmt.Printf("No records for %s on %s in remote API \n", country, date)
					noDataCountry["country"] = append(noDataCountry["country"], country)
					noDataCountry["date"] = append(noDataCountry["date"], date)
				} else {
					fmt.Println(response.StringencyData.DateValue, response.StringencyData.CountryCode, response.StringencyData.Confirmed)
					db.WriteDatabase(response)
				}
			}
		}
	}
	return noDataCountry
}

func apiGet(url string) models.ApiResponce {
	resp, getErr := http.Get(url)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	dataObject := models.ApiResponce{}
	jsonErr := json.Unmarshal(body, &dataObject)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}
	return dataObject
}

func DatesRange(start, end time.Time) (datesRange []string) {
	for ; !start.After(end); start = start.AddDate(0, 0, 1) {
		datesRange = append(datesRange, start.Format("2006-01-02"))
	}
	return datesRange
}

//check if record for country on date is not present
func CheckDBForRecord(country string, date string) bool {
	result := models.DataInput{}
	search := db.DB.Where(&models.DataInput{DateValue: date, CountryCode: country}).First(&result)
	if search.RowsAffected == 0 {
		//fmt.Printf("No records for %s on %s \n", country, date)
		return true
	} else {
		return false
	}
}
