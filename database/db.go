package database

import (
	"fmt"

	"gitlab.com/my-epam-diploma/backend/config"
	"gitlab.com/my-epam-diploma/backend/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

//Connection to local db
func ConnectDatabase(config *config.Config) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Etc/UTC",
		config.DB.Host,
		config.DB.Username,
		config.DB.Password,
		config.DB.Name,
		config.DB.Port)
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	database.AutoMigrate(&models.DataInput{})

	DB = database
}

// Write to DB
func WriteDatabase(response models.ApiResponce) {
	input := models.DataInput{
		DateValue:        response.StringencyData.DateValue,
		CountryCode:      response.StringencyData.CountryCode,
		Confirmed:        response.StringencyData.Confirmed,
		Deaths:           response.StringencyData.Deaths,
		StringencyActual: response.StringencyData.StringencyActual,
		Stringency:       response.StringencyData.Stringency}
	//result := DB.Where(&models.DataInput{DateValue: response.StringencyData.DateValue, CountryCode: response.StringencyData.CountryCode}).First(&input)
	//if result.RowsAffected == 0 {
	DB.Create(&input)
	//}
}
