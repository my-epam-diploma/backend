package main

import (
	"gitlab.com/my-epam-diploma/backend/config"
	db "gitlab.com/my-epam-diploma/backend/database"
	"gitlab.com/my-epam-diploma/backend/server"
)

func main() {
	config := config.GetConfig()
	db.ConnectDatabase(config)
	server.StartServer()
}
