# backend

Backend. Written in GO.

## usefull commands
launch local Postgres for testing
```bash
docker run -it --rm --name gorm-postgres -e POSTGRES_DB=gorm -e POSTGRES_USER=gorm -e POSTGRES_PASSWORD=gorm -v C:\Users\Igor\postgres:/var/lib/postgresql/data -p 5432:5432 postgres:14.2
```
build and launch local backend container for testing
```bash
docker build . -t backend
```
```bash
docker run -it --rm --name backend --network="host" -e DB_HOST=localhost -e DB_PORT=5432 -e DB_USER=gorm -e DB_PASSWORD=gorm -e DB_NAME=gorm -p 8080:8080 backend
```

